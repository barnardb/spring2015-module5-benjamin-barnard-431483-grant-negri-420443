<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

// day, month, year

if(isset($_REQUEST['day']) && preg_match('/^\d{1,2}$/', $_REQUEST['day'])){
    $day = $_REQUEST['day'];
    if($day < 10){
        $day = "0".$day;
    }
} else {
    die("-1");
}

if(isset($_REQUEST['month']) && preg_match('/^(0?[1-9]|1[012])$/', $_REQUEST['month'])){
    $month = "".$_REQUEST['month'];
    if($month < 10){
        $month = "0".$month;
    }
} else {
    die("-1");
}

if(isset($_REQUEST['year']) && preg_match('/^\d{4}$/', $_REQUEST['year'])){
    $year = "".$_REQUEST['year'];
} else {
    die("-1");
}

$loggedIn = (isset($_SESSION['username']));

if($loggedIn != 1){
    // the user is not logged in, don't allow them to post
    echo "-1";
    exit;
} else if (isset($month) && isset($year) && isset($day)) {
    $stmt = $mysqli->prepare("SELECT `event_id`,`date`,`time`,`title`,`desc` FROM `events` WHERE `date` LIKE ? AND `user_id`=? ORDER BY `time` ASC ");
    if(!$stmt){
        die("-1");
    }
    $dateStr = "" . $year . "-" . $month . "-" . $day;
    // Bind the parameters
    $stmt->bind_param('ss', $dateStr, $un);
    $un = $_SESSION['username'];
    if($stmt->execute()){
        // return the raw HTML that should be displayed
        $result = $stmt->get_result();
        $stmt->close();
        $ids = [];
        $i = 0;
        while($row = $result->fetch_assoc()){
            $subject = $row['title']." on ".$row['date']." at ".$row['time'];
            printf("<p>
                        <input type='text' id='title_%s' value='%s' />
                    <p>
                        <input type='date' id='date_%s' value='%s' /> &nbsp;
                        <input type='time' id='time_%s' value='%s' />
                    </p>
                    <p>
                        <textarea id='desc_%s'>%s</textarea>
                    </p>
                    <p>
                        <button type='button' id='edit_%s' class='edit_btn'>Edit</button>
                        <button type='button' id='delete_%s' class='delete_btn'>Delete</button>
                        <a href='mailto:?subject=%s&amp;body=%s' target='_blank'><button type='button' class='sendemail'>Share</button></a>
                    </p>",
                    $row['event_id'], $row['title'], $row['event_id'], $row['date'], $row['event_id'], $row['time'], $row['event_id'], $row['desc'], $row['event_id'], $row['event_id'], $subject, $row['desc']);
            $ids[$i] = $row['event_id'];
            $i++;
        }
    } else {
        die("-1");
    }
}
?>
