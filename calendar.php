<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();
?>
<!doctype html>
<html>
<head>
 
  <meta charset="utf-8">
  <title>Calendarrr, Mateys</title>
  <link rel="stylesheet" type="text/css" href="./calendar.css" />
  <script>
    function clearCalendar(){
        for(var i = 1; i < 7; i++){
            for(var j = 1; j < 8; j++) {
                document.getElementById(i+"-"+j).className = "";
            }
        }
    }
    function displayEvents() {
        var day;
        if(document.getElementsByClassName("selected")[0].innerHTML.length <= 2 ){
          day = document.getElementsByClassName("selected")[0].innerHTML;
        } else {
          day = document.getElementsByClassName("selected")[0].childNodes[0].innerHTML;
        }
        var xhr = new XMLHttpRequest();
        xhr.addEventListener("load", displaySucceed, false);
        xhr.open("POST", "./getdayevents.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("day="+day+"&month="+currentMonth+"&year="+currentYear);
    }
    function displaySucceed(event) {
        if (event.target.responseText == "-1") {
            //err
        } else {
        document.getElementById("dispevents").innerHTML = event.target.responseText;
      
        var edits = document.getElementsByClassName("edit_btn");
        var deletes = document.getElementsByClassName("delete_btn");
        for(var i = 0; i < edits.length; i++){
          edits[i].addEventListener("click", handleEdit, false);
          deletes[i].addEventListener("click", handleDelete, false);
        }
        }
    }
    function sendemail(event) {
        //document.getElementsByClassName("sendemail"    
    }
    function handleEdit(event) {
    var id = event.target.id.substring(5);
    var xhr = new XMLHttpRequest();
    
    xhr.addEventListener("load", editSucceed, false);
    
    xhr.open("POST", "./edit.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var title = document.getElementById("title_" + id).value;
    var mmddyy = document.getElementById("date_" + id).value;
    var time = document.getElementById("time_" + id).value;
    var desc = document.getElementById("desc_" + id).value;
    var token = document.getElementById("token").value;
    xhr.send("token="+token+"&id="+id+"&title="+title+"&desc="+desc+"&date="+mmddyy+"&time="+time);
    }
    function handleDelete(event) {
    var id = event.target.id.substring(7);
    var xhr = new XMLHttpRequest();
    var token = document.getElementById("token").value;
    
    xhr.addEventListener("load", deleteSucceed, false);
    
    xhr.open("POST", "./delete.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("token="+token+"&id="+id);
    }
    function editSucceed(event){
    displayEvents();
    if (event.target.responseText == "1") {
      alert("Your event has been modified!");
    } else {
      alert(event.target.responseText);
    }
    }
    function deleteSucceed(event){
    displayEvents();
    }
    function dateClicked(event) {
        var t;
        if (event.target == '[object HTMLTableCellElement]') {
            t = event.target;
        } else {
            t = event.target.parentElement;
        }
        if (t.innerHTML.length > 0) {
            clearCalendar();
            
            t.className = "selected";
            
            var today = -1;
            var tDate = new Date();
            
            if (tDate.getMonth() + 1 == currentMonth && tDate.getFullYear() == currentYear) {
                today = new Date().getDate();
                var d = new Date(currentYear, currentMonth - 1, 1);
                var startD = d.getDay();
                for(var i = 1; i < 6; i++){
                    for(var j = 1; j < 8; j++) {
                        var day = i * 7 + j - 7;
                        if (day - startD == today && document.getElementById(i+"-"+j).className != "selected") {
                            document.getElementById(i+"-"+j).className = "today";
                        }
                    }
                }
            }
        displayEvents();
        highlightMonthEvents();
        }
    }
    function setCalendarTitle(month, year) {
        var title = document.getElementById("monthyear");
        var titleText;
        switch(month){
            case 1:
                titleText = "January";
                break;
            case 2:
                titleText = "February";
                break;
            case 3:
                titleText = "March";
                break;
            case 4:
                titleText = "April";
                break;
            case 5:
                titleText = "May";
                break;
            case 6:
                titleText = "June";
                break;
            case 7:
                titleText = "July";
                break;
            case 8:
                titleText = "August";
                break;
            case 9:
                titleText = "September";
                break;
            case 10:
                titleText = "October";
                break;
            case 11:
                titleText = "November";
                break;
            case 12:
                titleText = "December";
                break;
        }
        title.innerHTML = titleText + " " + year;
    }
    function setCalendarInfo(month, year) {
        var today = -1;
        var tDate = new Date();
        
        if (tDate.getMonth() + 1 == month && tDate.getFullYear() == year) {
                today = new Date().getDate();
        }
        var d = new Date(year, month - 1, 1);
        var days = new Date(year, month, 0).getDate();
        var startD = d.getDay();
        for(var i = 1; i < 7; i++){
            var day = i * 7 - 6;
            if (day - startD > days) {
                document.getElementById("r"+i).style.display = 'none';
            } else {
                document.getElementById("r"+i).style.display = '';
            }
            for(var j = 1; j < 8; j++) {
                var elem = document.getElementById(i+"-"+j);
                elem.addEventListener("click", dateClicked);
                var day = i * 7 + j - 7;
                if (day - startD > 0 && day - startD <= days) {
                    if (day - startD < today || currentYear < tDate.getFullYear() || (currentYear == tDate.getFullYear() && currentMonth - 1 < tDate.getMonth())) {
                        elem.innerHTML = (day - startD);
                    } else if (day - startD >= today) {
                        elem.innerHTML = "<span class='future'>"+(day - startD)+"</span>";
                        if (day - startD == today) {
                            elem.className = "today";
                        }
                    }
                } else {
                        elem.innerHTML = "";
                }
            }
        }
        
        
        
        setCalendarTitle(month, year);
    highlightMonthEvents();
    }
    function highlightMonthEvents(){
      if(loggedIn) {
    var xhr = new XMLHttpRequest();
      
      xhr.addEventListener("load", highlightEvents, false);

      xhr.open("POST", "./getmonthevents.php", true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.send("month="+currentMonth+"&year="+currentYear);
      }
    }
    function highlightEvents(event) {
        if (event.target.responseText == "-1") {
            //err
        } else {
            var data = JSON.parse(event.target.responseText);
            for (var i=0; i<data.length; i++) {
                var day = data[i].date.split("-")[2];
                
                var d = new Date(currentYear, currentMonth-1, 1);
                var startD = d.getDay();
                
                var k = new Date(currentYear, currentMonth-1, day).getDay()+1;
                var j = 0;//Math.floor((day-k) / 7)+1;
                if (7 - k <= startD && Math.floor((day-k) / 7) >= 0) {
                  j = 1;
                }
                j += Math.floor((day-k) / 7) + 1;
                if (document.getElementById(j+"-"+k).className != "selected") {
                  document.getElementById(j+"-"+k).className = "hasevent";
                }
            }
        }
    }   
    function nextClicked(event) {
        currentMonth++;
        if (currentMonth > 12) {
            currentMonth = 1;
            currentYear++;
        }
        clearCalendar();
        setCalendarInfo(currentMonth, currentYear);
    }
    function prevClicked(event) {
        currentMonth--;
        if (currentMonth < 1) {
            currentMonth = 12;
            currentYear--;
        }
        clearCalendar();
        setCalendarInfo(currentMonth, currentYear);
    }
    function login(event) {
        var uname = document.getElementById("uname").value;
        var upass = document.getElementById("upass1").value;
        var xhr = new XMLHttpRequest();
        
        xhr.addEventListener("load", loginSucceed, false);
        
        xhr.open("POST", "./login.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("username="+uname+"&password="+upass);
    }
    function loginSucceed(event) {
        if(event.target.responseText == "1") {
            // We should hide the login area and show the username
        loggedIn = true;
            document.getElementById("loginform").style.display = "none";
            document.getElementById("logoutbtn").style.display = "";
            document.getElementById("newevent").style.display = "";
            document.getElementById("logout").addEventListener("click", logout);
            document.getElementById("nuevoevento").addEventListener("click", nuevoevento);
            setCalendarInfo(currentMonth, currentYear);
        }
    }
    function register(event) {
        var upass1 = document.getElementById("upass1").value;
        var upass2 = document.getElementById("upass2").value;
        if (upass1 == upass2) {
            var uname = document.getElementById("uname").value;
            //add user to table
            var xhr = new XMLHttpRequest();
            xhr.addEventListener("load", registerSucceed, false);
            xhr.open("POST", "./register.php", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.send("username="+uname+"&password="+upass1+"&password2="+upass2);
        } else {
            //throw err
            alert("The entered passwords must match.");
            document.getElementById("upass1").value = "";
            document.getElementById("upass2").value = "";
        }
  
    }
    function registerSucceed(event) {
        if(event.target.responseText == "1") {
      loggedIn = true;
            document.getElementById("loginform").style.display = "none";
            document.getElementById("logoutbtn").style.display = "";
            document.getElementById("newevent").style.display = "";
            document.getElementById("logout").addEventListener("click", logout);
            document.getElementById("nuevoevento").addEventListener("click", nuevoevento);
            setCalendarInfo(currentMonth, currentYear);
        }
    }
    function logout(event) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "./logout.php", true);
        xhr.addEventListener("load", logoutSucceed, false);
        xhr.send();
    }
    function logoutSucceed(event) {
    loggedIn = false;
        document.getElementById("loginform").style.display = "";
        document.getElementById("logoutbtn").style.display = "none";
        document.getElementById("newevent").style.display = "none";
        document.getElementById("login").addEventListener("click", login);
        document.getElementById("register").addEventListener("click", register);
    clearCalendar();
    setCalendarInfo(currentMonth, currentYear);
    document.getElementById("dispevents").innerHTML = "";
    }
    function nuevoevento(event) {
        var title = document.getElementById("title").value;
        var desc = document.getElementById("desc").value;
        var mmddyy = document.getElementById("date").value;
        var time = document.getElementById("time").value;
        var xhr = new XMLHttpRequest();
        
        xhr.addEventListener("load", createSucceed, false);
        
        //need to make newevent.php
        xhr.open("POST", "./newevent.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("title="+title+"&desc="+desc+"&date="+mmddyy+"&time="+time);
    }
    function createSucceed(event) {
        //refresh newEvent
        if (event.target.responseText == 1) {
            alert("New event added.");
        displayEvents();
        highlightMonthEvents();
        } else {
            alert(event.target.responseText);
        }
        document.getElementById("title").value = "";
        document.getElementById("desc").value = "";
        document.getElementById("date").value = "";
        document.getElementById("time").value = "";
    }
    var currentMonth = new Date().getMonth() + 1;
    var currentYear = new Date().getFullYear();
    var loggedIn = false;
    function init(event) {
        loggedIn = (document.getElementById("logoutbtn").style.display == "");
    
    clearCalendar();
        
        // month (1-12)
        // year (2015)
        setCalendarInfo(currentMonth, currentYear);
        document.getElementById("previousMonth").addEventListener("click", prevClicked);
        document.getElementById("nextMonth").addEventListener("click", nextClicked);
        
        if(loggedIn){
            document.getElementById("logout").addEventListener("click", logout);
            document.getElementById("nuevoevento").addEventListener("click", nuevoevento);
        } else {
            document.getElementById("login").addEventListener("click", login);
            document.getElementById("register").addEventListener("click", register);
        }
    }
    document.addEventListener("DOMContentLoaded", init, false);
    </script>
</head>
 
<body>
    <input type="hidden" id="token" value="<?php echo $_SESSION['token']; ?>" />
    <div>
        <header>
            <p><h2>
                <button type="button" id="previousMonth"><<</button> &nbsp;
                <div id="monthyear"></div> &nbsp;
                <button type="button" id="nextMonth">>></button>
            </h2></p>

        </header>
    </div>
    <div id="dispevents" class="disp">
        
    </div>
    <div>
        <table>
            <tr>
                <td>S</td>
                <td>M</td>
                <td>Tu</td>
                <td>W</td>
                <td>Th</td>
                <td>F</td>
                <td>S</td>
            </tr>
            <tr id="r1">
                <td id="1-1"></td>
                <td id="1-2"></td>
                <td id="1-3"></td>
                <td id="1-4"></td>
                <td id="1-5"></td>
                <td id="1-6"></td>
                <td id="1-7"></td>
            </tr>
            <tr id="r2">
                <td id="2-1"></td>
                <td id="2-2"></td>
                <td id="2-3"></td>
                <td id="2-4"></td>
                <td id="2-5"></td>
                <td id="2-6"></td>
                <td id="2-7"></td>
            </tr>
            <tr id="r3">
                <td id="3-1"></td>
                <td id="3-2"></td>
                <td id="3-3"></td>
                <td id="3-4"></td>
                <td id="3-5"></td>
                <td id="3-6"></td>
                <td id="3-7"></td>
            </tr>
            <tr id="r4">
                <td id="4-1"></td>
                <td id="4-2"></td>
                <td id="4-3"></td>
                <td id="4-4"></td>
                <td id="4-5"></td>
                <td id="4-6"></td>
                <td id="4-7"></td>
            </tr>
            <tr id="r5">
                <td id="5-1"></td>
                <td id="5-2"></td>
                <td id="5-3"></td>
                <td id="5-4"></td>
                <td id="5-5"></td>
                <td id="5-6"></td>
                <td id="5-7"></td>
            </tr>
            <tr id="r6">
                <td id="6-1"></td>
                <td id="6-2"></td>
                <td id="6-3"></td>
                <td id="6-4"></td>
                <td id="6-5"></td>
                <td id="6-6"></td>
                <td id="6-7"></td>
            </tr>
        </table>
    </div>
    
    <div id="newevent" style="display:<?php echo (isset($_SESSION['token']) ? '' : 'none'); ?>">
        <p>
            <input type="text" id="title" placeholder="Event Title" />
        </p>
        <p>
            <input type="date" id="date" /> &nbsp;
            <input type="time" id="time" />
        </p>
        <p>
            <textarea id="desc" placeholder="Description"></textarea>
        </p>
        <p>
            <button type="button" id="nuevoevento">Create Event</button>
        </p>
    </div>
   
    <div id="loginform" style="display:<?php echo (isset($_SESSION['token']) ? 'none' : ''); ?>">
        <p>
            <input type="text" name="uname" id="uname" min="1" max="32" placeholder="Username" />
        </p>
        <p>
            <input type="password" name="upass1" id="upass1" placeholder="Password" />
            <button type="button" id="login">Log In</button>
        </p>
        <p>
            <input type="password" name="upass2" id="upass2" min="6" placeholder="Retype Password" />
            <button type="button" id="register">Register</button>
        </p>
    </div>
    
    <div id="logoutbtn" style="display:<?php echo (isset($_SESSION['token']) ? '' : 'none'); ?>">
        <p>
            <button type="button" id="logout">Log Out</button>
        </p>
    </div>

 
</body>
</html>
