<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

if(isset($_POST['username']) && preg_match('/^[\w_\-]+$/', $_POST['username'])){    
    $username = $_POST['username'];
} else if(isset($_POST['username'])) {
    die("The username you entered was blank or contained invalid characters.");
}

if(isset($_POST['password']) && preg_match('/^[\w_\-]+$/', $_POST['password'])){    
    $pwd_guess = $_POST['password'];
} else if(isset($_POST['password'])) {
    die("The password you entered was blank or contained invalid characters.");
}

if(isset($_SESSION['username'])){
    // the user is already logged in, don't allow them to log in
    die("You are already logged in!");
} else if (isset($username) && isset($pwd_guess)) { 
    // Select securePW from database where username is $_POST['username']
    $stmt = $mysqli->prepare("SELECT COUNT(*), id, crypted_password FROM users WHERE username=?");
    if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
    }
 
    // Bind the parameters
    $stmt->bind_param('s', $username);
    $stmt->execute();
    
    // Bind the results
    $stmt->bind_result($cnt, $user_id, $pwd_hash);
    $stmt->fetch();
    $stmt->close();
    
    // Compare the submitted password to the actual password hash
    if($cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
        // Login succeeded!
        $_SESSION['username'] = $user_id;
        $_SESSION['token'] = substr(md5(rand()), 0, 10);
        die("1");
    }else{
        die("The username and password combination you entered was not found. Please try again.");
    }
}
?>