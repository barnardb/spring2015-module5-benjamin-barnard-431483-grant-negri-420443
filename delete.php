<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

// token, id
if(!isset($_POST['token']) || $_POST['token'] != $_SESSION['token']){
    die("-1");
}

if(isset($_POST['id']) && preg_match('/^\d+$/', $_POST['id'])){
    $id = "".$_POST['id'];
} else {
    die("-1");
}

$loggedIn = (isset($_SESSION['username']));

if($loggedIn != 1){
    // the user is not logged in, don't allow them to delete
    echo "-1";
    exit;
} else if (isset($id)) {
    $stmt = $mysqli->prepare("DELETE FROM `events` WHERE `event_id`=? AND `user_id`=?");
    if(!$stmt){
        die("-1");
    }
    // Bind the parameters
    $stmt->bind_param('ss', $eid, $un);
    $eid = $id;
    $un = $_SESSION['username'];
    if($stmt->execute()){
        die("1");
    } else {
        die("-1");
    }
}
?>