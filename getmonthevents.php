<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

// month, year

if(isset($_POST['month']) && preg_match('/^(0?[1-9]|1[012])$/', $_POST['month'])){    
    $month= $_POST['month'];
    if($month < 10){
        $month = "0".$month;
    }
} else if(isset($_POST['month'])) {
    die("-1");
}

if(isset($_POST['year']) && preg_match('/^\d{4}$/', $_POST['year'])){    
    $year = $_POST['year'];
} else if(isset($_POST['year'])) {
    die("-1");
}

if(!isset($_SESSION['username'])){
    // the user is not logged in, don't allow them to post
    die("-1");
} else if (isset($month) && isset($year)) {
    $stmt = $mysqli->prepare("SELECT DISTINCT `date` FROM `events` WHERE `date` LIKE ? AND `user_id`=? ORDER BY `date` ASC ");
    if(!$stmt){
        die("-1");
    }
    $dateStr = "" . $year . "-" . $month . "%";
    // Bind the parameters
    $stmt->bind_param('ss', $dateStr, $un);
    $un = $_SESSION['username'];
    if($stmt->execute()){
        $result = $stmt->get_result();
        
        $ret = [];
        $i = 0;
        while($row = $result->fetch_assoc()){  
            $ret[$i] = $row;
            $i++;
        }
        die(json_encode($ret));
    } else {
        die("-1");
    }
}
?>