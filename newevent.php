<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

//this is the login form which we can modify to be a newevent form
//this script expects title, desc, date, time and IMPLIES username from session

if(isset($_POST['title']) && preg_match('/[^\r]+/', $_POST['title'])){    
    $title= $_POST['title'];
} else if(isset($_POST['title'])) {
    die("The title you entered was blank or contained invalid characters.");
}

if(isset($_POST['desc']) && preg_match('/[^\r]+/', $_POST['desc'])){    
    $desc = $_POST['desc'];
} else if(isset($_POST['desc'])) {
    // Switch comments to make description required
    $desc = "";
    // die("The description you entered was blank or contained invalid characters.");
}

if(isset($_POST['date']) && preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $_POST['date'])){    
    $date= $_POST['date'];
} else if(isset($_POST['date'])) {
    die("The date you entered was blank or contained invalid characters.".$_POST['date']);
}

if(isset($_POST['time']) && preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/', $_POST['time'])){    
    $time= $_POST['time'];
} else if(isset($_POST['time'])) {
    die("The time you entered was blank or contained invalid characters.".$_POST['time']);
}

if(!isset($_SESSION['username'])){
    // the user is not logged in, don't allow them to post
    die("You are not logged in!");
} else if (isset($title) && isset($desc) && isset($date) && isset($time)) {
    $stmt = $mysqli->prepare("INSERT INTO `events`(`event_id`,`user_id`, `date`, `time`,`title`,`desc`) VALUES (NULL,?,?,?,?,?)");
    if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
    }
    
    // Bind the parameters
    $stmt->bind_param('sssss', $un, $date, $time, $title, $desc);
    $un = $_SESSION['username'];
    if($stmt->execute()){
        die("1");
    } else {
        die("0");
    }
}
?>