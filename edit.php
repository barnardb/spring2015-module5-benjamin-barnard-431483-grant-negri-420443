<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();

// token, id, date, time, title, desc

if(!isset($_POST['token']) || $_POST['token'] != $_SESSION['token']){
    die("-1");
}

if(isset($_POST['id']) && preg_match('/^\d+$/', $_POST['id'])){
    $id = "".$_POST['id'];
} else {
    die("-1");
}

if(isset($_POST['date']) && preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $_POST['date'])){
    $date = "".$_POST['date'];
} else {
    die("-1");
}

if(isset($_POST['time']) && preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/', $_POST['time'])){
    $time = "".$_POST['time'];
} else {
    die("-1");
}

if(isset($_POST['title']) && preg_match('/[^\r]+/', $_POST['title'])){
    $title = "".$_POST['title'];
} else {
    die("-1");
}

if(isset($_POST['desc']) && preg_match('/[^\r]+/', $_POST['desc'])){
    $desc = "".$_POST['desc'];
} else {
    die("-1");
}

$loggedIn = (isset($_SESSION['username']));

if($loggedIn != 1){
    // the user is not logged in, don't allow them to delete
    echo "-1";
    exit;
} else if (isset($id) && isset($date) && isset($time) && isset($title) && isset($desc)) {
    $stmt = $mysqli->prepare("UPDATE `events` SET `date`=?,`time`=?,`title`=?,`desc`=? WHERE `event_id`=? AND `user_id`=?");
    if(!$stmt){
        die("-1");
    }
    // Bind the parameters
    $stmt->bind_param('ssssss', $date, $time, $title, $desc, $eid, $un);
    $eid = $id;
    $un = $_SESSION['username'];
    if($stmt->execute()){
        die("1");
    } else {
        die("-1");
    }
}
?>