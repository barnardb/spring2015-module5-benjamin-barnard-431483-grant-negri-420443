<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();


if(isset($_POST['username']) && preg_match('/^[\w_\-]+$/', $_POST['username'])){    
    $username = $_POST['username'];
} else if(isset($_POST['username'])) {
    die("The username you entered was blank or contained invalid characters.");
}

if(isset($_POST['password']) && preg_match('/^[\w_\-]+$/', $_POST['password'])){    
    $new_pwd = $_POST['password'];
} else if(isset($_POST['password'])) {
    die("The password you entered was blank or contained invalid characters.");
}

if(isset($_POST['password2']) && preg_match('/^[\w_\-]+$/', $_POST['password2'])){    
    $new_pwd2 = $_POST['password2'];
}

if(isset($new_pwd) && isset($new_pwd2) && $new_pwd != $new_pwd2) {
    die("The passwords you entered did not match.");
}



if(isset($_SESSION['username'])){
    // the user is already logged in, don't allow them to register
    die("You are already logged in!");
} else if (isset($username) && isset($new_pwd) && isset($new_pwd2) && $new_pwd == $new_pwd2) { 
    // Select the number of users in the database where username is $_POST['username']
    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");
    if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
    }
    
 
    // Bind the parameters
    $stmt->bind_param('s', $username);
    $stmt->execute();
     
    // Bind the results
    $stmt->bind_result($cnt);
    $stmt->fetch();
    $stmt->close();
    
    // Check to make sure the username is not already taken
    if($cnt == 0){
        // The username is available, register the user
        // Create a salt-hash for the user
        $crypted_pwd = crypt($new_pwd);
        
        $stmt2 = $mysqli->prepare("INSERT INTO `users` (`id`,`username`,`crypted_password`) VALUES (NULL, ?, ?);");
        if(!$stmt2){
                printf("Query Prep Failed2: %s\n", $mysqli->error);
                exit;
        }
        
        // Bind the parameters
        $stmt2->bind_param('ss', $un, $crypted_pwd);
        $un = $username;
        $stmt2->execute();
        $stmt2->close();
        
        // Attempt to log the user in
        // Select securePW from database where username is $_POST['username']
        $stmt3 = $mysqli->prepare("SELECT COUNT(*), id, crypted_password FROM users WHERE username=?");
        if(!$stmt3){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
        }
        
        
        // Bind the parameter
        $stmt3->bind_param('s', $username);
        $stmt3->execute();
        
        // Bind the results
        $stmt3->bind_result($cnt, $user_id, $pwd_hash);
        $stmt3->fetch();
        $stmt3->close();
        
        // Compare the submitted password to the actual password hash
        if( $cnt == 1 && crypt($new_pwd, $pwd_hash)==$pwd_hash){
            // Login succeeded!
            $_SESSION['username'] = $user_id;
            $_SESSION['token'] = substr(md5(rand()), 0, 10);
            
            // Done
            die("1");
        }else{
            $stmt3->close();
            die("There was an error with your registration. Please try again.");
        }
    }else{
        die("The username that you requested is already in use, please try again.");
    }
}
?>